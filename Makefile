all:
	$(MAKE) linux
	mv uds uds.linux_amd64
	$(MAKE) mac
	mv uds uds.osx
	$(MAKE) pi
	mv uds uds.linux_arm
	$(MAKE) windows

linux:
	GOOS="linux" GOARCH="amd64" go build uds.go

pi:
	GOOS="linux" GOARCH="arm" go build uds.go

mac:
	GOOS="darwin" GOARCH="amd64" go build uds.go

windows:
	GOOS="windows" GOARCH="amd64" go build -o uds.exe uds.go
