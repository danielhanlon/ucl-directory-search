package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
)

//I is iris
//U is UPI
type ucl_MetaData struct {
	Title      string `json:"a"`
	Name       string `json:"B"`
	Surname    string `json:"s"`
	Email      string `json:"E"`
	Tel        string `json:"M"`
	Extension  string `json:"W"`
	Department string `json:"7"`
	UPI        string `json:"U"`
	Type       string `json:"g"`
	OrgHtml    string `json:"Z"`
}
type Result struct {
	Title    string       `json:"title"`
	MetaData ucl_MetaData `json:"metaData"`
}
type ResultPacket struct {
	Results []Result `json:"results"`
}
type Response struct {
	MyResultPacket ResultPacket `json:"resultPacket"`
}
type Answer struct {
	MyResponse Response `json:"response"`
}

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "Usage: uds <search term>\n")
		os.Exit(1)
	}
	query := url.QueryEscape(strings.Join(os.Args[1:], " "))
	resp, err := http.Get("http://search2.ucl.ac.uk/s/search.json?collection=website-meta&profile=_directory&tab=directory&num_ranks=1000&query=" + query)
	if err != nil {
		log.Fatal(err)
	}

	dec := json.NewDecoder(resp.Body)
	var m Answer
	parse_err := dec.Decode(&m)
	if parse_err != nil {
		log.Fatal(parse_err)
	}

	people := m.MyResponse.MyResultPacket.Results
	var r ucl_MetaData
	for p := range people {
		r = people[p].MetaData
		fmt.Printf("Name\t: %s\n", strings.TrimSpace(fmt.Sprintf("%s %s %s", r.Title, r.Name, r.Surname)))
		fmt.Printf("Dept\t: %s\n", r.Department)
		fmt.Printf("Email\t: %s\n", r.Email)
		if r.Type != "" {
			fmt.Printf("Type\t: %s\n", r.Type)
		}
		if r.Extension != "" {
			fmt.Printf("Ext\t: %s\n", r.Extension)
		}
		if r.UPI != "" {
			fmt.Printf("UPI\t: %s\n", r.UPI)
		}
		if p < (len(people) - 1) {
			fmt.Println()
		}
	}
}
